# Библиотека на ALF Linkedlist
Библиотека която реализира версия на единично свързан списък, колекция от елементи, съставена от серия от възли, която позволява много ефективно добавяне и премахване на елементи.

Повече информация https://bitbucket.org/gonzoveliki/kofa/src/master/

## Инструкции за употреба
Всеки възлов елемент съдържа данни от шаблонния тип и препратка към следващия възел. По този начин е възможно да добавяте или премахвате елементи чрез създаване или премахване на възли и управление на препратките между тях.

Класът има функции за получаване, добавяне, вмъкване, замяна и премахване на елементи от LinkedList. Получаването на елемент е сравнително бавен процес в сравнение с обикновен масив, тъй като е необходимо да се премине през всички възли, докато се достигне до желания възел.

За да подобри ефективността, класът запазва препратка към последния елемент, до който е достъпен. Това избягва необходимостта да преминавате през колекцията от самото начало, което е особено полезно при многократен достъп до един и същ индекс.

Освен това, класът поддържа препратка към възел главата и опашката, което позволява ефективно прилагане на LIFO (Stack) и FIFO (Tail) структури.

Съществуват и методи за търсене на елементи в LinkedList, инвертиране на елементите и копиране на елементите към и от външен масив.

### Конструктор
Класът LinkedList се инстанцира чрез неговия конструктор.
```c++
// Създава нов LinkedList
LinkedList();
```

### Общи методи
```c++
// Връща размера
int getSize() const;
```

### Елементи за достъп
```c++
// Връща първия елемент
T getHead();

// Връща последния елемент
T getTail();

// Връща елемента в посочената позиция
T getAt(int index);
```

### Вмъкване на елементи
```c++
// Вмъкване на елемент в началото
void insertHead(T);

// Вмъкване на елемент в края
void insertTail(T);

// Вмъкване на елемент в посочената позиция
void insertAt(int index, T);
```

### Замяна на елементи
```c++
// Замяна на първия елемент
void replaceHead(T value);

// Замяна на последния елемент
void replaceTail(T value);

// Замяна на елемента в посочената позицияa
void replaceAt(int index, T value);
```

### Изтриване на елементи
```c++
// Изтриване на първия елемент
T removeHead();

// Изтриване на последния елемент
T removeTail();

// Изтриване на елемента в посочената позиция
T removeAt(int index);

// Премахване на всички елементи
void clear();
```

### Преобразуване в масиви
```c++
// Връща всички елементи в нов масив
T* toArray();

// Създава нов LinkedList, като копира елементите от масив
void fromArray(T* fromArray, int n);
```

## Примери
Библиотеката LinkedList включва следните примери за илюстриране на нейното използване.

* LinkedList: Общ пример за използване на класа LinkedList.

```c++
#include "ALF_LinkedListLib.h"

#define ser Serial

// Извежда стойностите на масив
void printArray(int* x, int length) {
  for (int i = 0; i < length; i++) {
    if (i) ser.write(',');
    ser.print(x[i]);
  }
  ser.println();
}

// Извежда стойностите от свързан списък
void printLinkedList(LinkedList<int> &list) {
  ser.println(list.getHead());
  for (int i = 0; i < 5; i++)
    ser.println(list.getAt(i));
  ser.println(list.getTail());
}

void setup() {
  ser.begin(1000000);

  // Create LinkedList
  LinkedList<int> linkedlist;

  // Вмъкване
  linkedlist.insertHead(1);	//1
  linkedlist.insertTail(5);	//1,5
  linkedlist.insertAt(0, 2);//1,2,5
  linkedlist.insertAt(1, 4);//1,2,4,5
  linkedlist.insertAt(1, 3);//1,2,3,4,5

  // Отпечатва стойностите от списъка
  printLinkedList(linkedlist);

  // Замества
  linkedlist.replaceHead(10);
  linkedlist.replaceAt(2, 30);
  linkedlist.replaceAt(3, 40);
  linkedlist.replaceAt(1, 20);
  linkedlist.replaceTail(50);

  // Отпечатва стойностите от списъка
  printLinkedList(linkedlist);

  // Записва в масив
  int* toArray = linkedlist.toArray();
  printArray(toArray, linkedlist.getSize());

  // Зарежда от масив
  int testArray[] = { 100, 200, 300, 400, 500 };
  linkedlist.fromArray(testArray, sizeof(testArray) / sizeof(int));
  int* toArray2 = linkedlist.toArray();
  printArray(toArray2, linkedlist.getSize());
}

void loop() {
}
```