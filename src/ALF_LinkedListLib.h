#ifndef ALF_LinkedListLib_h // Библиотеката на АЛФ за 
#define ALF_LinkedListLib_h // единично свързан списък

/// Клас свързан списък
template <typename T> class LinkedList {
protected: // Защитени
	struct Node { /// Структура на възлов елемент
		T value; // Стойност от шаблонен тип Т
		Node *next; // Връзка към следващ възлов елемент
		Node(T v = 0, Node *n = nullptr) : value(v), next(n) { }
	};
	int size, iterInd; // Размер na списъка и индекс на итератора
	Node *head, *tail, *iter; // Глава, опашка и итератор
public: // Публични
	LinkedList() { // Конструктор създаващ нов свързан списък
		head = tail = nullptr, size = 0; // Нулиране на глава, опашка и размер
	};
	~LinkedList() { // Деструктор заличаващ инстанцията на свързаният списък
		Node *node;
		while (head != nullptr)
			node = head, head = head->next, delete node;
		tail = nullptr, size = 0;
	}
/// ### Общи методи
	int getSize() const { // Връща размера
		return size;
	}
	T getHead() { // Връща първия елемент
		return head->value;
	}
	T getTail()	{ // Връща последния елемент
		return tail->value;
	}
	T getAt(int i) { // Връща елемента в посочената позиция
		Node *node = getNode(i);
		return node->value;
	}
/// ### Вмъкване на елементи
	void insertHead(T v) { // Вмъкване на елемент в началото
		if (size == 0)
			return insertEmpty(v);
		Node *node = new Node(v, head);
		head = node, iterReset(), size++;
	}
	void insertTail(T v) { // Вмъкване на елемент в края
		if (size == 0)
			return insertEmpty(v);
		Node *node = new Node(v);
		tail->next = node, tail = node, iterReset(), size++;
	}
	void insertAt(int i, T v) { // Вмъкване на елемент в посочената позиция
		if (size == 0)
			return insertEmpty(v);
		Node *node = new Node(v), * prev = getNode(i);
		node->next = prev->next, prev->next = node;
		if (iterInd >= i)
			iterReset();
		size++;
	}
/// ### Замяна на елементи
	void replaceHead(T v) { // Замяна на първия елемент
		head->value = v;
	}
	void replaceTail(T v) { // Замяна на последния елемент
		tail->value = v;
	}
	void replaceAt(int i, T v) { // Замяна на елемента в посочената позиция
		getNode(i)->value = v;
	}
/// ### Изтриване на елементи
	T removeHead() { // Изтриване на първи елемент
		if (size <= 0)
			return T();
		if (size > 1) {
			Node* next = head->next;
			T rst = head->value;
			delete(head), head = next, size--;
			return rst;
		}
		else
			return T();
	}
	T removeTail() { // Изтриване на последен елемент
		if (size <= 0)
			return T();
		if (size > 1) {
			Node *prev = getNode(size - 2);
			T rst = prev->next->value;
			delete(prev->next), prev->next = nullptr, tail = prev, size--;
			return rst;
		}
		else {
			T rst = head->value;
			delete(head), head = tail = nullptr, size = 0;
			return rst;
		}
	}
	T removeAt(int i) { // Изтриване на елемента в посочената позиция
		if (size <= 0)
			return T();
		Node *prev = getNode(i - 1), *node = prev->next;
		prev->next = node->next, delete node, size--;
	}
	void clear() { // Премахване на всички елементи
		while (size > 0)
			removeTail();
	}
/// ### Преобразуване от и към масив
	void fromArray(T *fromArray, int n) { // Създава нов LinkedList, като копира елементите от масив
		if (size > 0)
			clear();
		for (int iCount = 0; iCount < n; iCount++)
			insertTail(fromArray[iCount]);
	}
	T *toArray() { // Връща всички елементи в нов масив
		T *rst = new T[size];
		Node *node = head;
		for (int iCount = 0; iCount < size; iCount++)
			rst[iCount] = node->value, node = node->next;
		return rst;
	}
private:
	void insertEmpty(T v) { // Вмъква празен възел
		Node *node = new Node(v);
		head = tail = node, size++;
	}
	void iterReset() { // Нулира итератора и започва от начало (главата)
		iterInd = 0, iter = head;
	}
	void iterMove(int i) { // Премества итератора към следващят
		if (iterInd > i)
			iterReset();
		while (iterInd < i && iterInd < size)
			iter = iter->next, iterInd++;
	}
	inline Node *getNode(int i) { // Връща елемент с пореден номер
		iterMove(i);
		return iter;
	}
};
#endif
