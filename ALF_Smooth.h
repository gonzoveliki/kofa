#ifndef ALF_Smooth
#define ALF_Smooth

template <typename T = long> class Smooth {
protected:
    const unsigned char len;
    T *buf, sum;
    unsigned char cnt, ind;
public:
    Smooth(const unsigned char l) : len(l < 100 ? l : 100), buf(new T[len]) {
        clear();
    }
    void clear() {
        cnt = 0, ind = 0, sum = 0;
        for (unsigned char i = 0; i < len; i++)
            buf[i] = 0;
    }
    inline bool full() { return  cnt == len; }
    bool feed(T data) {
        sum -= buf[ind], sum += buf[ind] = data;
        if (++ind == len)
            ind = 0;
        if (cnt < len)
            cnt++;
        return full();
    }
    T average() {
        return cnt ? (sum / (T)((cnt < len) ? cnt : len)) : 0;
    }
    T average(T data) {
        feed(data);
        return average();
    }
};

#endif
