#include "ALF_LinkedListLib.h"

#define ser Serial

// Извежда стойностите на масив
void printArray(int* x, int length) {
  for (int i = 0; i < length; i++) {
    if (i) ser.write(',');
    ser.print(x[i]);
  }
  ser.println();
}

// Извежда стойностите от свързан списък
void printLinkedList(LinkedList<int> &list) {
  ser.println(list.getHead());
  for (int i = 0; i < 5; i++)
    ser.println(list.getAt(i));
  ser.println(list.getTail());
}

void setup() {
  ser.begin(1000000);

  // Create LinkedList
  LinkedList<int> linkedlist;

  // Вмъкване
  linkedlist.insertHead(1);	//1
  linkedlist.insertTail(5);	//1,5
  linkedlist.insertAt(0, 2);//1,2,5
  linkedlist.insertAt(1, 4);//1,2,4,5
  linkedlist.insertAt(1, 3);//1,2,3,4,5

  // Отпечатва стойностите от списъка
  printLinkedList(linkedlist);

  // Замества
  linkedlist.replaceHead(10);
  linkedlist.replaceAt(2, 30);
  linkedlist.replaceAt(3, 40);
  linkedlist.replaceAt(1, 20);
  linkedlist.replaceTail(50);

  // Отпечатва стойностите от списъка
  printLinkedList(linkedlist);

  // Записва в масив
  int* toArray = linkedlist.toArray();
  printArray(toArray, linkedlist.getSize());

  // Зарежда от масив
  int testArray[] = { 100, 200, 300, 400, 500 };
  linkedlist.fromArray(testArray, sizeof(testArray) / sizeof(int));
  int* toArray2 = linkedlist.toArray();
  printArray(toArray2, linkedlist.getSize());
}

void loop() {
}